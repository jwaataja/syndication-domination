project(
    'SyndicationDomination',
    'cpp',
    version: '0.0',
    meson_version: '>= 0.58.0',
    license: 'AGPL3',
    default_options: [
        'cpp_std=c++17',
        'optimization=3'
    ]
)

author = 'gabmus'
authorfullname = 'Gabriele Musco'
authoremail = 'gabmus@disroot.org'
domain = 'org'
app_id = '.'.join([
    domain,
    author,
    meson.project_name()
])
pretty_name = 'Syndication Domination'
python_module_name = 'syndom'

profile = get_option('profile')
opt_python_bindings = get_option('PYTHON_BINDINGS')
opt_to_json_binary = get_option('TO_JSON_BINARY')
opt_html_support = get_option('HTML_SUPPORT')

pugixml = dependency('pugixml')

dependencies = [pugixml]
defines = []

if opt_html_support
    dependencies += [dependency('tidy')]
    defines += ['-DHTML_SUPPORT=1']
endif

build_args = []
link_args = []

if profile == 'debug'
    build_args += [
        '-g'
    ]
else
    build_args += [
        '-DNDEBUG',  # disable assertions
    ]
endif

prefix = get_option('prefix') # should be /usr
bindir = get_option('bindir')
datadir = get_option('datadir')

sources = [
    'src/feed.cpp',
    'src/feed_item.cpp',
    'src/html.cpp',
    'src/opml.cpp',
    'src/opml_item.cpp',
]

if opt_to_json_binary
    project_target = executable(
        meson.project_name(),
        sources + ['src/main.cpp'],
        dependencies: dependencies,
        install: true,
        install_dir: join_paths(prefix, bindir),
        cpp_args: build_args + defines,
        link_args: link_args,
        include_directories: include_directories('src')
    )
endif

if opt_python_bindings
    python = import('python')
    py_installation = python.find_installation(
        'python3',
        modules: ['pybind11']
    )
    if not py_installation.found()
        error('No valid python3 binary found')
    endif
    pybind11 = dependency('pybind11')
    pythondir = py_installation.get_install_dir()

    py_installation.extension_module(
        python_module_name,
        sources + ['src/pybind.cpp'],
        dependencies: dependencies + [pybind11],
        install: true,
        install_dir: pythondir,
        cpp_args: build_args + defines,
        link_args: link_args,
        include_directories: include_directories('src')
    )
endif
